<?php

$a=10;
$b=3;

if (($a%2)==0){
    echo "punto 1";
}else{
    echo "punto 2";
}

echo "punto 3";

if(($b%2)==0){
    echo "punto 1";
}else{
    echo "punto 2";
}

echo "punto 3";

?>

<?php 

/**
 * Opcion 1
 */

// crear un array directamente
$vocales=[
    "a","e","i","o","u"
];

var_dump($vocales);

/**
 * opcion 2
 */

//crear un array con la funcion array
$vocales=array(
    "a","e","i","o","u"
);

var_dump($vocales);

